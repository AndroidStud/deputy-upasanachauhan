Deputy Mobile Challenge

Hi,

Welcome to the Deputy Mobile development challenge :)

This should take less than few hours to write.
You are allowed to use any library you need as long as you explain why.
Git usage with regular commits is MANDATORY.
You can use you own GitHub/GitLab/Bitbucket... if you like.

For Android, you must use Java (8 is a bonus) with Android Studio (minimum API 16).
For iOS, you must use Swift 3.
You are NOT allowed to use hybrid or cross­compile application frameworks (e.g.
PhoneGap, Xamarin).


If you have any questions, please email challenge@deputy.com.
At the end of the challenge, you must send an email to challenge@deputy.com with a clonable link to your repository or an archive of your repository.
We will generate the application from the sources.
You must also send your answers to the questions at the end of the challenge.

This challenge will test your ability to
- Make a simple Android application,
- Make network calls to an API,
- Use the device location services,
- Display a image from a web link,
Bonus:
- Use the device local database (offline mode).

The goal is to create an app that allow the user to
- Start and end a shift.
- See the current shift in progress (if any).
- See the list of previous shifts (master & details workflow). Each list element MUST display the JSON "image".
- The shift details must show the start time, end time (if available) and a map with the start location and end location (if available).
Bonus:
- The current shift status & previous shifts details must be available offline.

The UI & UX is up to you.

Here is the API documentation

URL: https://apjoqdqpi3.execute-api.us-west-2.amazonaws.com/dmc

Every request should have a HTTP header as follows:
"Authorization", "Deputy " + sha1
where sha1 MUST be generated ONLY ONCE by YOU and be the SAME during the whole challenge.
The sha1 MUST be your first name, for example `echo -n louis | sha1sum` (linux) or `echo -n louis | shasum` (OSx).

GET /business => Get business info
{
	"name": "Deputy", (string)
	"logo": "https://www.myob.com/au/addons/media/logos/deputy_logo_1.png" (string)
}
Note: Business info will always stay the same... 

POST /shift/start => Start a shift
{
	"time": "2017-01-17T06:35:57+00:00", (string, ISO 8601)
	"latitude": "0.00000", (string)
	"longitude": "0.00000" (string)
}
!! You can not start more than one shift. !!

POST /shift/end => End a shift
{
	"time": "2017-01-17T16:42:28+00:00", (string, ISO 8601)
	"latitude": "0.00000", (string)
	"longitude": "0.00000" (string)
}

GET /shifts => List of the previous shifts

'null' if no shifts recorded, otherwise

[
	{
		"id": 42, (int)
		"start": "2017-01-17T06:35:57+00:00", (string, ISO 8601)
		"end": "",
		"startLatitude": "0.00000", (string)
		"startLongitude": "0.00000", (string)
		"endLatitude": "",
		"endLongitude": "",
		"image": "https://unsplash.it/500/500/?random"

	},
	{
		"id": 42, (int)
		"start": "2017-01-16T06:35:57+00:00", (string, ISO 8601)
		"end": "2017-01-16T18:42:12+00:00", (string, ISO 8601)
		"startLatitude": "0.00000", (string)
		"startLongitude": "0.00000", (string)
		"endLatitude": "0.00000", (string)
		"endLongitude": "0.00000", (string)
		"image": "https://unsplash.it/500/500/?random"
	},

	...
]


Bonus:
Whatever you like :)
Portrait/Landscape support for Master/Details
Map with all the shifts location


Advices:
KISS, DRY, YAGNI



Questions:
How long did it take you to do? What was your biggest problem? (if any)
What do you think about the API?
If you had more time, what next feature would you develop?