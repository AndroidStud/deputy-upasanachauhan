package com.deputy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Display Current shift
 */
public class OneFragment extends Fragment implements View.OnClickListener, LocationListener {

    private Button btnStart = null;
    private Button btnEnd = null;
    private ProgressDialog mProgressDialog;
    RestAdapter radapter;
    ShiftInterface restInt;
    Location location;
    double longitude;
    double latitude;
    int currentShift = -1;
    private ImageView imgForApi;
    private TextView txtStartTime;
    private TextView txtEndTime;
    ImageView imgMap;
    FragmentTransaction ft;
    FragmentManager fm;
    Double startLatitude = 0.0;
    Double startLongitude = 0.0;
    Double endLatitude = 0.0;
    Double endLongitude = 0.0;

    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_one, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");

        btnStart = (Button) getView().findViewById(R.id.btnStart);
        btnEnd = (Button) getView().findViewById(R.id.btnEnd);
        imgForApi = (ImageView) getView().findViewById(R.id.imgForApi);
        txtStartTime = (TextView) getView().findViewById(R.id.txtStartTime);
        txtEndTime = (TextView) getView().findViewById(R.id.txtEndTime);
        imgMap = (ImageView) getView().findViewById(R.id.imgMap);

        radapter = new RestAdapter.Builder().setEndpoint(Constants.url).build();
        restInt = radapter.create(ShiftInterface.class);

        fetchCurrentShift();

        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        try {
            location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location == null) {
                // request location update!!
                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 100, this);

                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 100, this);
            } else {
                longitude = location.getLongitude();
                latitude = location.getLatitude();
            }
        } catch (SecurityException e) {
            longitude = 0.0;
            latitude = 0.0;
        }

        imgMap.setOnClickListener(this);
        btnStart.setOnClickListener(this);
        btnEnd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                showDialog();
                restInt.startTime(new StartEndTimeModel(Constants.getCurrentDateTime(), latitude + "", longitude + ""), new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                        hideDialog();
                        Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
                        if (s.equals("Start shift - All good")) {
                            fetchCurrentShift();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideDialog();
                    }
                });
                break;

            case R.id.btnEnd:
                showDialog();

                restInt.endTime(new StartEndTimeModel(Constants.getCurrentDateTime(), latitude + "", longitude + ""), new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                        hideDialog();
                        Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
                        if (s.equals("End shift - All good")) {
                            fetchCurrentShift();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideDialog();
                    }
                });
                break;

            case R.id.imgMap:
                if ((startLatitude != 0.0 && startLongitude != 0.0) || (endLatitude != 0.0 && endLongitude != 0.0)) {
                    fm = getActivity().getSupportFragmentManager();
                    ft = fm.beginTransaction();
                    ft.add(android.R.id.content, MapFragment.newInstance(startLatitude, startLongitude, endLatitude, endLongitude), "mapFragmentTag");
                    ft.commit();
                } else {
                    Toast.makeText(getActivity(), "Found no start and end latitude/longitude", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void fetchCurrentShift() {

        showDialog();
        restInt.getShifts(new Callback<List<ShiftModel>>() {
            @Override
            public void success(List<ShiftModel> shiftModels, Response response) {
                if (shiftModels.size() != 0) {
                    for (int i = 0; i < shiftModels.size(); i++) {
                        if (shiftModels.get(i).getEnd().length() == 0) {
                            currentShift = i;

                            startLatitude = Double.valueOf(shiftModels.get(currentShift).getStartLatitude());
                            startLongitude = Double.valueOf(shiftModels.get(currentShift).getStartLongitude());
                            endLatitude = Double.valueOf(shiftModels.get(currentShift).getEndLatitude());
                            endLongitude = Double.valueOf(shiftModels.get(currentShift).getEndLongitude());

                            break;
                        }
                    }

                    List<ShiftModel> saveShiftModel = new ArrayList<ShiftModel>(shiftModels);

                    ShiftModel shiftModelForList = new ShiftModel();
                    if (currentShift != -1) {
                        saveShiftModel.remove(currentShift);
                    }
                    shiftModelForList.setShiftModelArray(saveShiftModel);
                    saveDataInPersistentObject(shiftModelForList);
                }

                if (currentShift != -1) {

                    Glide.with(getActivity()).load(shiftModels.get(currentShift).getImage())
                            .placeholder(android.R.drawable.ic_dialog_alert)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgForApi);


                    txtStartTime.setText(Constants.getDataTimeHumanReadable(shiftModels.get(currentShift).getStart()));
                    txtEndTime.setText(Constants.getDataTimeHumanReadable(shiftModels.get(currentShift).getEnd()));
                    currentShift = -1;
                } else {
                    txtStartTime.setText("No shift in progress");
                    txtEndTime.setText("Press start to begin the shift");
                    startLongitude = 0.0;
                    startLatitude = 0.0;

                    endLongitude = 0.0;
                    endLatitude = 0.0;
                }

                hideDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                hideDialog();
            }
        });
    }

    /**
     * Save data in object in persistent storage.
     * @param shiftModelForList
     */
    private void saveDataInPersistentObject(ShiftModel shiftModelForList) {

        ObjectOutputStream objectOut = null;

        try {
            FileOutputStream fileOut = getActivity().openFileOutput("prevFile", Activity.MODE_PRIVATE);
            objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(shiftModelForList);
            fileOut.getFD().sync();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (objectOut != null) {
                try {
                    objectOut.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public void showDialog() {
        if (mProgressDialog != null && !mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    public void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}