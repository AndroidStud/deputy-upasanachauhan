package com.deputy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Show previous shifts. Pull to refresh to see latest data in offline mode also.
 */
public class TwoFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    ShiftModel shiftModelData;
    ShiftModel shiftModelDataToBeDisplayed;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    FragmentTransaction ft;
    FragmentManager fm;
    private EfficientAdapter mAdapter;

    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_two, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        shiftModelDataToBeDisplayed = getSavedData();
        mAdapter = new EfficientAdapter();
        if (shiftModelDataToBeDisplayed != null) {
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        }
    }

    public ShiftModel getSavedData()
    {
        ObjectInputStream objectIn = null;
        try
        {
            FileInputStream fileIn = getActivity().openFileInput("prevFile");
            objectIn = new ObjectInputStream(fileIn);
            shiftModelData = (ShiftModel)objectIn.readObject();
        } catch (FileNotFoundException e) {
            // Do nothing
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (objectIn != null) {
                try {
                    objectIn.close();
                } catch (IOException e) {
                }
            }
        }

        return shiftModelData;

    }

    @Override
    public void onRefresh() {
        shiftModelDataToBeDisplayed = getSavedData();

        if(recyclerView.getAdapter() == null)
        {
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        }
        else
        {
            mAdapter.notifyDataSetChanged();
        }
       // if(mAdapter.onAttachedToRecyclerView(recyclerView));
        swipeRefreshLayout.setRefreshing(false);
    }

    public class EfficientAdapter extends RecyclerView.Adapter<EfficientAdapter.MyViewHolder> {

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView imgShift;
            private TextView txtStartTime;
            private TextView txtEndTime;
            private ImageView imgMap;

            public MyViewHolder(View view) {
                super(view);
                imgShift = (ImageView) view.findViewById(R.id.imgShift);
                txtStartTime = (TextView) view.findViewById(R.id.txtStartTime);
                txtEndTime = (TextView) view.findViewById(R.id.txtEndTime);
                imgMap = (ImageView) view.findViewById(R.id.imgMap);

            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            holder.txtStartTime.setText(Constants.getDataTimeHumanReadable(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getStart()));
            holder.txtEndTime.setText(Constants.getDataTimeHumanReadable(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getEnd()));
            if(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getImage() != null)
            {
                Glide.with(getActivity())
                        .load(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getImage())
                        .placeholder(android.R.drawable.ic_dialog_alert)
                        .into(holder.imgShift);
            }
            else
            {
                holder.imgShift.setImageDrawable(null);
            }

            holder.imgMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if((Double.valueOf(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getStartLatitude()) != 0.0 && Double.valueOf(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getStartLongitude()) != 0.0) ||
                            (Double.valueOf(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getEndLatitude()) != 0.0 && Double.valueOf(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getEndLongitude()) != 0.0))
                    {
                        fm = getActivity().getSupportFragmentManager();
                        ft = fm.beginTransaction();
                        ft.add(android.R.id.content, MapFragment.newInstance(Double.valueOf(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getStartLatitude()),
                                Double.valueOf(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getStartLongitude()),
                                Double.valueOf(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getEndLatitude()),
                                Double.valueOf(shiftModelDataToBeDisplayed.getShiftModelList().get(position).getEndLongitude())),"mapFragmentTag");
                        ft.commit();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Found no start and end latitude/longitude", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return shiftModelDataToBeDisplayed.getShiftModelList().size();
        }
    }
}