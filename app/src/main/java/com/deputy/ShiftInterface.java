package com.deputy;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface ShiftInterface {

    @Headers("Authorization: Deputy 3597644e45a208697b9f81ded58effb967515d11")
    @GET("/business")
    void getBusiness(Callback<ShiftModel> callback);

    @Headers("Authorization: Deputy 3597644e45a208697b9f81ded58effb967515d11")
    @GET("/shifts")
    void getShifts(Callback<List<ShiftModel>> callback);

    @Headers({
            "Authorization: Deputy 3597644e45a208697b9f81ded58effb967515d11",
            "Content-Type: application/json" })
    @POST("/shift/start")
    void startTime(@Body StartEndTimeModel body, Callback<String> callback);

    @Headers({
            "Authorization: Deputy 3597644e45a208697b9f81ded58effb967515d11",
            "Content-Type: application/json" })
    @POST("/shift/end")
    void endTime(@Body StartEndTimeModel body, Callback<String> callback);
}
