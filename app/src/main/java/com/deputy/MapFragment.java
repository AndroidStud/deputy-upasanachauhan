package com.deputy;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A simple {@link Fragment} subclass.
 * {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {
    private MapView mapView;
    private GoogleMap googleMap;
    private static final String ARG_PARAM1 = "startLat";
    private static final String ARG_PARAM2 = "startLong";
    private static final String ARG_PARAM3 = "endLat";
    private static final String ARG_PARAM4 = "endLong";
    private Double mParam1;
    private Double mParam2;
    private Double mParam3;
    private Double mParam4;
    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getDouble(ARG_PARAM1);
            mParam2 = getArguments().getDouble(ARG_PARAM2);

            mParam3 = getArguments().getDouble(ARG_PARAM3);
            mParam4 = getArguments().getDouble(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mapView = (MapView) getView().findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;
        // Add a marker and move the camera
        if(mParam1 != 0.0 && mParam2 != 0.0)
        {
            LatLng latLng = new LatLng(mParam1, mParam2);
            googleMap.addMarker(new MarkerOptions().position(latLng));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        }

        if(mParam3 != 0.0 && mParam4 != 0.0)
        {
            LatLng latLng = new LatLng(mParam3, mParam4);
            googleMap.addMarker(new MarkerOptions().position(latLng));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        }
    }

    /**
     * A new instance of this fragment using the provided parameters.
     * @param startLatitude  Parameter 1.
     * @param startLongitude Parameter 2.
     * @param endLatitude  Parameter 3.
     * @param endLongitude Parameter 4.
     * @return A new instance of fragment MapFragment.
     */
    public static Fragment newInstance(Double startLatitude, Double startLongitude, Double endLatitude, Double endLongitude) {
        MapFragment mapFragment = new MapFragment();
        Bundle args = new Bundle();
        args.putDouble(ARG_PARAM1, startLatitude);
        args.putDouble(ARG_PARAM2, startLongitude);
        args.putDouble(ARG_PARAM3, endLatitude);
        args.putDouble(ARG_PARAM4, endLongitude);
        mapFragment.setArguments(args);
        return mapFragment;
    }
}