package com.deputy;

/**
 * Created by Upasana on 3/21/2017.
 */

public class StartEndTimeModel {

    String time;
    String latitude;
    String longitude;

    StartEndTimeModel(String time, String latitude, String longitude) {
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
