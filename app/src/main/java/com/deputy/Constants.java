package com.deputy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Upasana on 3/21/2017.
 */

public class Constants {
    public static final String url = " https://apjoqdqpi3.execute-api.us-west-2.amazonaws.com/dmc";

    /**
     * To send formatted date.
     * @return formattedDate
     */
    public static String getCurrentDateTime()
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+00:00");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    /**
     * Return string type of date from formatted date
     * @param date
     * @return String date
     */
    public static String getDataTimeHumanReadable(String date)
    {
        if(date != "")
        {
            try {
                return String.valueOf(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+00:00").parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {

            return "Shift in progress";
        }
        return "";

    }
}